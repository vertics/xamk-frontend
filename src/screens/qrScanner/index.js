import React, { Component } from 'react'
import {
  View,
  TouchableOpacity,
  Dimensions,
  Alert,
  Platform,
  StatusBar,
  BackHandler
} from 'react-native'
import { connect } from 'react-redux'
import QRCodeScanner from 'react-native-qrcode-scanner'
import { Text, Button, Input } from 'react-native-elements'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { StackActions, NavigationActions } from 'react-navigation'

import { startNewTrackAction } from '../../actions/startNewTrack'
import { $blue, text } from '../../style'
import CustomStatusBar from '../../components/statusBar'
import HeaderBar from '../../components/headerBar'

import fetchData from '../../utils/fetchData'

const { width, height } = Dimensions.get('window')

const StatusBarHeight = StatusBar.currentHeight

class QrScanner extends Component {

  state = {
    inputValue: 'ENTER CODE MANUALLY'
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
  }

  handleBackButton() {
    this.goBack()
    return true
  }

  goBack() {
    this.props.navigation.goBack()
  }

  async scanCode(e) {
    console.log(e)
    let trackData = await fetchData(e, 5000)
    console.log(trackData)
    // if fetch returns error return it
    if (trackData == 'An SSL error has occurred and a secure connection to the server cannot be made.') return Alert.alert('Error', 'Connection failed')
    if (typeof trackData == 'number' || trackData == 'The request timed out.') {
      if (trackData == 500) return Alert.alert('Error', 'No track found with given id')
      return Alert.alert(trackData == 'The request timed out.' ? 'Timeout' : 'Error ' + trackData, 'Failed fetching data')
    }

    let trackFound = false
    let i = 0

    while (!trackFound && i < trackData.length) {
      if (trackData[i].description.qrCode == e) {
        console.log(trackData[i].description.qrCode, e)
        trackData = trackData[i]
        trackFound = true
      }
      i++
    }

    if (trackData.length) {
      return Alert.alert('Error', 'No track found with given id')
    }

    this.props.startNewTrack(trackData)
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Map' })],
    })
    return this.props.navigation.dispatch(resetAction)
  }

  changeInputValue(value) {
    this.setState({ inputValue: value })
  }

  toggleFocus(boolean) {
    if (boolean) {
      if (this.state.inputValue == 'ENTER CODE MANUALLY') return this.setState({ inputValue: '' })
      return
    }
    if (!boolean) {
      if (this.state.inputValue == '') return this.setState({ inputValue: 'ENTER CODE MANUALLY' })
      return
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <CustomStatusBar />
        <HeaderBar
          title="SCAN QR-CODE"
          leftElement={{ icon: 'arrow-back', func: () => this.goBack() }}
        />
        <KeyboardAwareScrollView scrollEnabled={false}>
          <View style={styles.scannerContainer}>
            <QRCodeScanner
              onRead={(e) => this.scanCode(e.data)}
              checkAndroid6Permissions
              permissionDialogTitle={'Camera Permission'}
              permissionDialogMessage={'This app needs permission to use camera to work properly.'}
            />
          </View>
          <View style={styles.bottomContainer}>
            <View style={styles.textContainer}>
              <Text style={text.title} h4>SCAN QR-CODE TO START</Text>
            </View>
            <View style={styles.textContainer}>
              <Text style={text.title}>Doesn't work?</Text>
              <Input
                value={this.state.inputValue}
                onChangeText={(e) => this.changeInputValue(e)}
                onFocus={() => this.toggleFocus(true)}
                onBlur={() => this.toggleFocus(false)}
                inputStyle={text.title}
                labelStyle={text.title}
                containerStyle={styles.inputContainer}
                inputContainerStyle={styles.inputContainerStyle}
                keyboardType={'numeric'}
              />
              <View style={styles.iconContainer}>
                <Icon.Button
                  name={'send'}
                  color={'white'}
                  backgroundColor={'transparent'}
                  iconStyle={{ display: this.state.inputValue == 'ENTER CODE MANUALLY' ? 'none' : 'flex' }}
                  onPress={() => this.scanCode(this.state.inputValue)}
                  underlayColor={'transparent'}
                />
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: $blue.primary
  },
  scannerContainer: {
    width: '100%',
    height: width
  },
  bottomContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: height - width - (Platform.OS == 'ios' ? 70 : 70 - StatusBarHeight)
  },
  textContainer: {
    width: '100%',
    padding: 20
  },
  inputContainer: {
    width: '100%',
    backgroundColor: $blue.dark,
    borderRadius: 5,
    marginTop: 10
  },
  inputContainerStyle: {
    borderBottomWidth: 0
  },
  iconContainer: {
    position: 'absolute',
    bottom: 22,
    right: 10
  }
}

const mapDispatchToProps = dispatch => {
  return {
    startNewTrack: (data) => {
      dispatch(startNewTrackAction(data))
    }
  }
}

export default connect(null, mapDispatchToProps)(QrScanner)