import React, { Component } from 'react'
import {
  View,
  ScrollView,
  Alert
} from 'react-native'
import { connect } from 'react-redux'
import {
  Text,
  Card,
  Button
} from 'react-native-elements'
import Icon from 'react-native-vector-icons/MaterialIcons'

import { text, card, $blue } from '../../style'
import HeaderBar from '../../components/headerBar'
import CustomStatusBar from '../../components/statusBar'
import { resetTrackAction } from '../../actions/resetTrack'

class TrackCompletedScreen extends Component {

  componentDidMount() {
    if (this.props.wrongAnswers == null || this.props.numberOfQuestions == null) return Alert.alert('Error', 'Failed to load data, expect unstable behaviour.')
  }

  resetApp() {
    this.props.resetTrack()
    this.props.navigation.replace('MainScreen')
  }

  render() {
    const { wrongAnswers, numberOfQuestions } = this.props
    console.log(wrongAnswers, numberOfQuestions)
    return (
      <View style={styles.container}>
        <CustomStatusBar />
        <HeaderBar
          title="TRACK FINISHED"
        />
        <ScrollView>
          <Card containerStyle={card}>
            <View style={styles.iconOuterContainer}>
              <View style={styles.iconContainer}>
                <Icon name="location-city" color={$blue.dark} size={100} />
              </View>
            </View>
            <Text style={[text.title, text.darkBlue, styles.textMargin]} h2>TRACK FINISHED!</Text>
            <Text style={[text.title, text.darkBlue, styles.textMargin]}>You got {Math.round(((numberOfQuestions - wrongAnswers) / numberOfQuestions) * 100)}% of the questions right on first try</Text>
            <Button
              title="START NEW TRACK"
              titleStyle={text.title}
              buttonStyle={styles.btn}
              onPress={() => this.resetApp()}
            />
          </Card>
        </ScrollView>
      </View>
    )
  }
}

TrackCompletedScreen.defaultProps = {
  wrongAnswers: null,
  numberOfQuestions: null
}

const styles = {
  container: {
    flex: 1
  },
  textMargin: {
    marginTop: 10
  },
  iconOuterContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5
  },
  iconContainer: {
    width: 130,
    height: 130,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 3,
    borderColor: $blue.dark,
    borderRadius: 65
  },
  btn: {
		backgroundColor: $blue.light,
		marginTop: 20,
		paddingHorizontal: 10
	}
}

const mapStateToProps = state => {
  return {
    wrongAnswers: state.currentTrackStatus.wrongAnswers,
    numberOfQuestions: state.trackData.waypoints.length
  }
}

const mapDispatchToProps = dispatch => {
  return {
    resetTrack: () => {
      dispatch(resetTrackAction())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TrackCompletedScreen)
