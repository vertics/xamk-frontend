import React, { Component } from 'react'
import {
  View,
  Platform,
  TouchableOpacity,
  PermissionsAndroid,
  Alert
} from 'react-native'
import { Text, Button } from 'react-native-elements'
import LinearGradient from 'react-native-linear-gradient'
import SplashScreen from 'react-native-splash-screen'

import { $blue, text } from '../../style'

import CustomStatusBar from '../../components/statusBar'

class MainScreen extends Component {

  componentDidMount() {
    SplashScreen.hide()

    if (Platform.OS == 'android') {
      this.requestGpsPermission()
    }
  }

  async goToCamera() {
    this.props.navigation.navigate('QrScanner')
  }

  async requestGpsPermission() {
		console.log('requesting gps permission')
		try {
			const granted = await PermissionsAndroid.request(
				PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
				{
					'title': 'This app needs to access location',
					'message': 'Without location this app ' + 'wont\' work properly.'
				}
			)
			if (granted === PermissionsAndroid.RESULTS.GRANTED) {
				console.log("Granted location permission")
			} else {
        console.log("Location permission denied")
        Alert.alert('Error', 'Location access denied. This app wont work without it.')
			}
		} catch (err) {
			console.warn(err)
		}
	}

  render() {
    return (
      <LinearGradient colors={[$blue.light, $blue.dark]} style={styles.container}>
        <CustomStatusBar />

        <Text style={[text.logo]}>edu</Text>
        <Text style={text.logo}>Quiz</Text>

        <Text style={[text.basic, styles.text]}>Start your adventure by scanning the QR-code</Text>

        <View style={styles.btnContainer}>
          <Button
            borderRadius={5}
            title="SCAN QR-CODE"
            titleStyle={text.title}
            buttonStyle={styles.btn}
            onPress={() => this.goToCamera()}
          />
        </View>

      </LinearGradient>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    paddingTop: Platform.OS == 'ios' ? 20 : 0
  },
  text: {
    textAlign: 'center',
    marginTop: 30
  },
  btn: {
    backgroundColor: 'transparent',
    height: 45,
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 10,
    paddingHorizontal: 10
  },
  btnContainer: {
    alignItems: 'center',
    marginTop: 10
  }
}

export default MainScreen