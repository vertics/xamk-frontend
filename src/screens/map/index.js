import React, { Component } from 'react'
import {
  View,
  Vibration,
  Alert,
  BackHandler
} from 'react-native'
import { connect } from 'react-redux'
import MapView, { Marker } from 'react-native-maps'
import { Text } from 'react-native-elements'
import { StackActions, NavigationActions } from 'react-navigation'

import CustomStatusBar from '../../components/statusBar'
import MapMarker from './renderMarker'
import measureDistance from '../../utils/measureDistance'
import { waypointCompletedAction } from '../../actions/waypointCompleted'

import { text, $blue } from '../../style'

class MapScreen extends Component {

  state = {
    lat: null,
    lon: null
  }

  componentDidMount() {
    this.position()
    if (this.props.currentWaypoint = null || this.props.trackData == 'error') return Alert.alert('Error', 'Failed to load data, expect unstable behaviour.')
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
  }

  handleBackButton() {
    return true
  }

  position() {
		this.watchPosition = navigator.geolocation.watchPosition(
			async (position) => {
					const initialPosition = JSON.stringify(position)
          await this.setState({ lat: Number(position.coords.latitude), lon: Number(position.coords.longitude) })
          const waypointLocation = {
            lat: this.props.trackData.waypoints[this.props.currentWaypoint].location.latitude,
            lon: this.props.trackData.waypoints[this.props.currentWaypoint].location.longitude,
            radius: this.props.trackData.waypoints[this.props.currentWaypoint].location.radius
          }
          const distance = await measureDistance(position.coords.latitude, position.coords.longitude, waypointLocation.lat, waypointLocation.lon);

          console.log(waypointLocation, position.coords, distance)

					if (distance <= waypointLocation.radius) {
						this.correctPosition();
					}
			},
			(error) => console.log(error.message),
			{ enableHighAccuracy: true, distanceFilter: 1 }
		)
  }

  correctPosition() {
    navigator.geolocation.clearWatch(this.watchPosition)
    Vibration.vibrate(500)
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Waypoint' })],
    })
    return this.props.navigation.dispatch(resetAction)
  }

  render() {
    const { trackData, currentWaypoint } = this.props
    return (
      <View style={styles.container}>
        <CustomStatusBar themeColor="dark-content" backgroundColor="rgba(255, 255, 255, 0.3)" />
        <MapView
          style={styles.map}
          initialRegion={{
            latitude: Number(trackData.waypoints[currentWaypoint].location.latitude),
            longitude: Number(trackData.waypoints[currentWaypoint].location.longitude),
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421
          }}
          showsUserLocation
        >
        {
          trackData.waypoints.map((value, index) => {
            return <MapMarker key={index} coordinates={{ longitude: value.location.longitude, latitude: value.location.latitude }} name={value.name} index={index + 1} />
          })

        }
        </MapView>
        <View style={styles.infoContainer}>
          <View>
            <Text style={text.title} h2>{trackData.description.name.toUpperCase()}</Text>
            <Text style={text.title}>{currentWaypoint == 0 ? 'Go to first point to start track' : 'Go to point #' + Number(currentWaypoint + 1)}</Text>
          </View>
        </View>
      </View>
    )
  }
}

MapScreen.defaultProps = {
  trackData: 'error',
  currentWaypoint: null
}

const styles = {
  container: {
    flex: 1
  },
  map: {
    width: '100%',
    height: '100%'
  },
  infoContainer: {
    position: 'absolute',
    width: '100%',
    height: 100,
    bottom: 0,
    backgroundColor: $blue.lighter,
    flexDirection: 'column',
    justifyContent: 'center'
  }
}

const mapStateToProps = state => {
  return {
    trackData: state.trackData,
    currentWaypoint: state.currentTrackStatus.waypoint
  }
}

const mapDispatchToProps = dispatch => {
  return {
    waypointCompleted: () => {
      dispatch(waypointCompletedAction())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MapScreen)