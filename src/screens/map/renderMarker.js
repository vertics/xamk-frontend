import React, { Component } from 'react'
import {
  View,
  Text
} from 'react-native'
import { Marker } from 'react-native-maps'
import Icon from 'react-native-vector-icons/MaterialIcons'

import { text } from '../../style'

const MapMarker = ({ coordinates = {}, name = '', index = null }) => {
  return (
    <Marker
      coordinate={{ latitude: Number(coordinates.latitude), longitude: Number(coordinates.longitude) }}
      title={name}
    >
      <View style={styles.textContainer}>
        <Text style={[styles.text, text.black]}>{index}</Text>
      </View>
      <Icon
        name="place"
        color="#E2423C"
        backgroundColor="transparent"
        size={50}
      />
    </Marker>
  )
}

const styles = {
  textContainer: {
    position: 'absolute',
    backgroundColor: 'white',
    borderRadius: 11,
    height: 22,
    width: 22,
    zIndex: 100,
    top: 8,
    left: 14
  },
  text: {
    textAlign: 'center',
    lineHeight: 22
  }
}

export default MapMarker