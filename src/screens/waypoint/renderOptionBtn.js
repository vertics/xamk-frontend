import React, { Component } from 'react'
import { Button } from 'react-native-elements'

import { text, $blue } from '../../style'

const OptionBtn = ({ question = "", index = null, checkAnswer = () => {} }) => {
  return (
    <Button
      title={question}
      titleStyle={text.title}
      buttonStyle={[styles.btn, { backgroundColor: backgroundColors[index] }]}
      onPress={() => checkAnswer(index)}
    />
  )
}

const backgroundColors = [$blue.darker, $blue.dark, $blue.primary, $blue.light, $blue.lighter]

const styles = {
  btn: {
    height: 38,
    marginVertical: 4
  }
}

export default OptionBtn