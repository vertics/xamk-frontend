import React, { Component } from 'react'
import {
	View,
	ScrollView,
	Animated,
	Platform,
	Dimensions,
	Alert,
	BackHandler
} from 'react-native'
import { connect } from 'react-redux'
import {
	Card,
	Text,
	Button
} from 'react-native-elements'
import Modal from 'react-native-modal'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { StackActions, NavigationActions } from 'react-navigation'

import { $blue, card, text } from '../../style'
import { waypointCompletedAction } from '../../actions/waypointCompleted'
import { addWrongAnswerAction } from '../../actions/addWrongAnswer'
import CustomStatusBar from '../../components/statusBar'
import HeaderBar from '../../components/headerBar'
import OptionBtn from './renderOptionBtn'
import { imageUrl } from '../../constants'

const { width, height } = Dimensions.get('window')

class WaypointScreen extends Component {

	state = {
		questionContainerHeight: new Animated.Value(0),
		btnContainerHeight: new Animated.Value(60),
		modalCorrect: false,
		modalWrong: false,
		firstTimeAnswering: true
	}

	componentDidMount() {
		if (this.props.waypointIndex == null || this.props.waypointData == 'error' || this.props.numberOfWaypoints == null) return Alert.alert('Error', 'Failed to load data, expect unstable behaviour.')
		BackHandler.addEventListener('hardwareBackPress', this.handleBackButton)
	}

  componentWillUnmount() {
		console.log('unmounting...')
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
  }

  handleBackButton() {
    return true
  }

	setPopupHeight(e) {
		if (this.state.popupHeight == 0) {
			this.setState({ popupHeight: e.nativeEvent.layout.height, popupTop: new Animated.Value(-e.nativeEvent.layout.height - (Platform.OS == 'ios' ? 20 : 0)) })
		}
		return
	}

	openQuestion() {
		Animated.parallel([
			Animated.timing(
				this.state.questionContainerHeight,
				{
					toValue:  51 + this.props.waypointData.options.length * 46,
					duration: 300
				}
			),
			Animated.timing(
				this.state.btnContainerHeight,
				{
					toValue: 0,
					duration: 300
				}
			)
		]).start()
	}

	checkAnswer(index) {
		if (index == this.props.waypointData.correctAnswer) {
			return this.setState({ modalCorrect: true })
		}
		return this.setState({ modalWrong: true })
	}

	resetQuestions() {
		this.setState({ modalWrong: false })
		if (this.state.firstTimeAnswering) {
			this.props.addWrongAnswer()
			this.setState({ firstTimeAnswering: false })
		}
	}

	toMap() {
		this.setState({ modalCorrect: false })
		if (this.props.waypointIndex + 1 >= this.props.numberOfWaypoints) {
			return setTimeout(() => {
				return this.props.navigation.navigate('TrackCompleted')
			}, 500)
		}
		this.props.waypointCompleted()
		return setTimeout(() => {
			const resetAction = StackActions.reset({
				index: 0,
				actions: [NavigationActions.navigate({ routeName: 'Map' })],
			})
			return this.props.navigation.dispatch(resetAction)
		}, 500)
	}

	render() {
		const { waypointIndex, waypointData } = this.props

		return (
			<View style={styles.container}>
				<CustomStatusBar />
				<HeaderBar
					title={((waypointIndex + 1).toString() + '. ' + waypointData.name).toUpperCase()}
				/>
				<ScrollView>
					<Card
						containerStyle={card}
						image={{ uri: imageUrl + waypointData.image }}
					>
						<Text style={[text.title, text.black, styles.waypointName]} h4>{waypointData.name.toUpperCase()}</Text>
						<Text style={[text.basic, text.black]}>{waypointData.description}</Text>
						<Animated.View style={{ height: this.state.btnContainerHeight }}>
							<Button
								title="OPEN QUESTION"
								titleStyle={text.title}
								buttonStyle={styles.btn}
								onPress={() => this.openQuestion()}
							/>
						</Animated.View>
						<Animated.View style={[styles.questionContainer, { height: this.state.questionContainerHeight }]}>
							<Text style={[text.basic, styles.questionLabelText]}>QUESTION</Text>
							<View style={styles.questionDivider} />
							<Text style={[text.basic, text.black, styles.questionText]}>{waypointData.question}</Text>
							{
								waypointData.options.map((value, index) => {
									return <OptionBtn question={value} index={index} checkAnswer={(e) => this.checkAnswer(e)} key={index} />
								})
							}
						</Animated.View>
					</Card>
				</ScrollView>

				<Modal
					isVisible={this.state.modalCorrect}
					animationIn="slideInDown"
					animationOut="slideOutUp"
				>
					<View style={styles.modal}>
						<View style={styles.iconContainer}>
							<Icon name="check" color={$blue.dark} size={50} />
						</View>
						<Text style={[text.title, text.darkBlue]} h4>CORRECT ANSWER!</Text>
						<Button
							title={this.props.waypointIndex + 1 >= this.props.numberOfWaypoints ? 'CONTINUE' : 'OPEN MAP'}
							titleStyle={text.title}
							buttonStyle={styles.btnModal}
							onPress={() => this.toMap()}
						/>
					</View>
				</Modal>

				<Modal
					isVisible={this.state.modalWrong}
					animationIn="slideInDown"
					animationOut="slideOutUp"
				>
					<View style={styles.modal}>
						<View style={styles.iconContainer}>
							<Icon name="close" color={$blue.dark} size={50} />
						</View>
						<Text style={[text.title, text.darkBlue]} h4>WRONG ANSWER</Text>
						<Button
							title="TRY AGAIN"
							titleStyle={text.title}
							buttonStyle={styles.btnModal}
							onPress={() => this.resetQuestions()}
						/>
					</View>
				</Modal>

			</View>
		)
	}
}

WaypointScreen.defaultProps = {
	waypointIndex: null,
	waypointData: 'error',
	numberOfWaypoints: null
}

const styles = {
	container: {
		flex: 1
	},
	waypointName: {
		textAlign: 'left',
		marginBottom: 3
	},
	btn: {
		backgroundColor: $blue.light,
		marginTop: 20,
		paddingHorizontal: 10
	},
	questionContainer: {
		width: '100%',
		overflow: 'hidden',
		backgroundColor: 'white',
		marginTop: 20
	},
	questionLabelText: {
		color: $blue.dark
	},
	questionDivider: {
		height: 1,
		backgroundColor: $blue.dark,
		width: '100%',
		marginBottom: 10
	},
	questionText: {
		marginBottom: 5
	},
	modal: {
		backgroundColor: 'white',
		padding: 20,
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 5
	},
	iconContainer: {
		height: 60,
		width: 60,
		borderWidth: 3,
		borderRadius: 30,
		borderColor: $blue.dark,
		alignItems: 'center',
		justifyContent: 'center',
		marginBottom: 15
	},
	btnModal: {
		backgroundColor: $blue.dark,
		marginTop: 15,
		paddingHorizontal: 10
	}
}

const mapStateToProps = state => {
	return {
		waypointIndex: state.currentTrackStatus.waypoint,
		waypointData: state.trackData.waypoints[state.currentTrackStatus.waypoint],
		numberOfWaypoints: state.trackData.waypoints.length
	}
}

const mapDispatchToProps = dispatch => {
	return {
		waypointCompleted: () => {
			dispatch(waypointCompletedAction())
		},
		addWrongAnswer: () => {
			dispatch(addWrongAnswerAction())
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(WaypointScreen)