export const startNewTrackAction = (data) => ({
  type: 'START_NEW_TRACK',
  data: data
})