import axios from 'axios'

import { trackFetch } from '../constants'

export default function fetchData(track_id, timeout) {
  const url = trackFetch
  return axios.get(url, {
    timeout: 5000
  })
  .then((response) => {
    console.log(response)
    return response.data
  })
  .catch((error) => {
    if (error.response) {
      console.log(error.response)
      return error.response.status
    } else if (error.request) {
      return error.request._response
    } else {
      return 'unknown error'
    }
  });

}