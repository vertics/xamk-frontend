export default function measureDistance(currentLat, currentLon, distanceLat, distanceLon) {
  var R = 6378.137 // Radius of earth in KM
  var dLat = distanceLat * Math.PI / 180 - currentLat * Math.PI / 180
  var dLon = distanceLon * Math.PI / 180 - currentLon * Math.PI / 180
  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
  Math.cos(currentLat * Math.PI / 180) * Math.cos(distanceLat * Math.PI / 180) *
  Math.sin(dLon/2) * Math.sin(dLon/2)
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
  var d = R * c
  var distance = d * 1000 // meters

  return distance
}