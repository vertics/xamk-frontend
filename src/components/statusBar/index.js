import React from 'react'
import { StatusBar } from 'react-native'

const CustomStatusBar = ({ themeColor = 'light-content', backgroundColor = '#0F2030' }) => {
  return (
    <StatusBar
      backgroundColor={backgroundColor}
      barStyle={themeColor}
      animated
    />
  )
}

export default CustomStatusBar