import React from 'react'
import { Header } from 'react-native-elements'

import { $blue } from '../../style'

const HeaderBar = ({ leftElement = {}, rightElement = {}, title = 'EduQuiz', colors = { text: '#FFF', bg: $blue.dark } }) => {
  return (
    <Header
      leftComponent={{ icon: leftElement.icon, color: colors.text, underlayColor: 'transparent', onPress: leftElement.func }}
      centerComponent={{ text: title, style: { color: colors.text } }}
      rightComponent={{ icon: rightElement.icon, color: colors.text, underlayColor: 'transparent', onPress: rightElement.func }}
      backgroundColor={colors.bg}
    />
  )
}

export default HeaderBar