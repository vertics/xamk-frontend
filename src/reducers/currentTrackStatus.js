const initialState = {
  waypoint: 0,
  wrongAnswers: 0
}

const currentTrackStatus = (state = initialState, action) => {
  switch (action.type) {
    case 'WAYPOINT_COMPLETED':
      return { waypoint: Number(state.waypoint + 1), wrongAnswers: state.wrongAnswers }

    case 'WRONG_ANSWER_COUNTER':
      return { waypoint: state.waypoint, wrongAnswers: Number(state.wrongAnswers + 1) }

    case 'RESET_TRACK':
      return initialState

    default:
      return state
  }
}

export default currentTrackStatus