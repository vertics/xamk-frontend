import { combineReducers } from 'redux'

/*import selectedTrack from './selectedTrack';
import nav from './nav';
import currentTrack from './currentTrack';

const frontendStore = combineReducers({
  selectedTrack,
  nav,
  currentTrack
});*/

import trackData from './trackData'
import currentTrackStatus from './currentTrackStatus'

const appStore = combineReducers({
  trackData,
  currentTrackStatus
})

export default appStore