const initialState = {
    description: {
      name: 'Test track',
      qrCode: 974222564141
    },
    waypoints: [
      {
        location: {
          latitude: 60.188535,
          longitude: 24.830506,
          radius: 30
        },
        options: [
          'Option #1',
          'Option #2',
          'Option #3'
        ],
        _id: '5afd8303ae6e1b27fe8f3b18',
        name: 'A Grid',
        description: 'Placeholder text here',
        image: 'https://placeimg.com/640/480/arch',
        question: 'Question here',
        correctAnswer: 2
      },
      {
        location: {
          latitude: 60.18846,
          longitude: 24.83292,
          radius: 30
        },
        options: [
          'testi123',
          'Option #2',
          'Option #3'
        ],
        _id: '5afd8303ae6e1b27fe8f3b17',
        name: 'Second point',
        description: 'sadglijnölj lkj sal ölk jklsad fö lijasfd',
        image: 'https://placeimg.com/640/480/any',
        question: 'asdf',
        correctAnswer: 2
      },
      {
        location: {
          latitude: 60.18766,
          longitude: 24.8316,
          radius: 30
        },
        options: [
          'testi123',
          'Option #2',
          'Option #3'
        ],
        _id: '5afd8303ae6e1b27fe8f3b16',
        name: 'third point',
        description: 'sadglijnölj lkj sal ölk jklsad fö lijasfd',
        image: 'https://placeimg.com/640/480/any',
        question: 'asdf',
        correctAnswer: 2
      }
    ],
    _id: '5afd8303ae6e1b27fe8f3b15',
    __v: 0,
    id: '5afd8303ae6e1b27fe8f3b15'
}

const trackData = (state = initialState, action) => {
  switch (action.type) {
    case 'START_NEW_TRACK':
      return action.data

    default:
      return state
  }
}

export default trackData