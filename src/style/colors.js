export const $blue = {
  darker: '#13293D',
  dark: '#16324F',
  primary: '#194A72',
  light: '#2A628F',
  lighter: '#3F88C5'
}