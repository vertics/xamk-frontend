import { $blue, $errorRed } from './colors'
import { text, card } from './style'

export {
  $blue,
  $errorRed,
  text,
  card
}