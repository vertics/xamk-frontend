import { Platform } from 'react-native'

export const text = {
  title: {
    color: 'white',
    textAlign: 'center',
    fontWeight: '200'
  },
  basic: {
    color: 'white',
    fontWeight: '200'
  },
  logo: {
    color: 'white',
    fontFamily: Platform.OS == 'ios' ? 'Comfortaa' : 'Comfortaa-Regular',
    textAlign: 'center',
    fontSize: 80,
    lineHeight: 100
  },
  black: {
    color: 'black'
  },
  darkBlue: {
    color: '#16324F'
  }
}

export const card = {
  margin: 8
}