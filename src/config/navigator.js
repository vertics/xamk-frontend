import React from 'react'

import { createStackNavigator } from 'react-navigation'

import MainScreen from '../screens/mainScreen'
import QrScanner from '../screens/qrScanner'
import MapScreen from '../screens/map'
import WaypointScreen from '../screens/waypoint'
import TrackCompletedScreen from '../screens/trackCompleted'

const Navigator = createStackNavigator(
  {
    MainScreen: {
      screen: MainScreen,
    },
    QrScanner: {
      screen: QrScanner
    },
    Map: {
      screen: MapScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    Waypoint: {
      screen: WaypointScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    TrackCompleted: {
      screen: TrackCompletedScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    }
  },

  // Route config
  {
    initialRouteName: 'MainScreen',
    headerMode: 'none',
  }
)

export default Navigator