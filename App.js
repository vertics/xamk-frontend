import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import { addNavigationHelpers } from 'react-navigation'

import appStore from './src/reducers'
import Navigator from './src/config/navigator'

const store = createStore(appStore);

class App extends Component {
  /*componentDidMount() {
    SplashScreen.hide()
  }*/

  render() {
    return (
      <Provider store={store}>
        <Navigator />
      </Provider>
    )
  }
}

export default App